package com.ysd.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ysd.entity.Admin;
import com.ysd.entity.Student;
import com.ysd.entity.Teacher;
import com.ysd.service.AdminService;
import com.ysd.service.DepartmentService;
import com.ysd.service.ReadroomService;
import com.ysd.service.SectionService;
import com.ysd.service.StudentService;
import com.ysd.service.TeacherService;
import com.ysd.util.Result;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private ReadroomService readroomService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private StudentService studnetService;
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private SectionService sectionService;

	@RequestMapping("jumpLogin")
	public String jumpLogin(Model model) {
		model.addAttribute("readroomList", readroomService.findReadroomAll());
		model.addAttribute("msg", "欢迎来到本系统");
		return "WeAdmin/login";
	}

	@RequestMapping("jumpAdminLogin")
	public String jumpAdminLogin(Model model) {
		model.addAttribute("msg", "欢迎来到管理员登录");
		return "adminlogin";
	}

	@RequestMapping("jumpIndex")
	public String jumpIndex(Model model, HttpServletRequest request,Admin admin) {
		Result result=adminService.adminLogin(admin);
		HttpSession session = request.getSession();
		if(result.getSuccess()) {
			session.setAttribute("admin", result.getAdmin());
			model.addAttribute("admin", result.getAdmin());
			return "WeAdmin/index";
		}
		else {
			model.addAttribute("msg", result.getMsg());
			return "adminlogin";
		}
		
	}

	@RequestMapping("jumpStudentList")
	public String jumpStudentList(Model model) {
		model.addAttribute("departmentList", departmentService.findDepartmentAll());
		return "WeAdmin/pages/student/studentList";
	}

	@RequestMapping("jumpTeacherList")
	public String jumpTeacherList(Model model) {
		model.addAttribute("sectionList", sectionService.findSectionAll());
		return "WeAdmin/pages/teacher/teacherList";
	}

	@RequestMapping("jumpWelcome")
	public String jumpWelcome() {
		return "WeAdmin/pages/welcome";
	}

	@RequestMapping("/login")
	public String login(Model model, HttpServletRequest request, String cardid, Integer readroomid) {
		Result result = adminService.login(cardid, readroomid);
		HttpSession session = request.getSession();
		if (result.getSuccess()) {
			if (result.getStudent() != null) {
				session.setAttribute("student", result.getStudent());
				model.addAttribute("student", result.getStudent());
				model.addAttribute("name", result.getStudent().getStudent_name());
				model.addAttribute("cardid", result.getStudent().getStudent_cardid());
				model.addAttribute("msg", result.getMsg());
				return "index";
			} else {
				session.setAttribute("teacher", result.getTeacher());
				model.addAttribute("teacher", result.getTeacher());
				model.addAttribute("name", result.getTeacher().getTeacher_name());
				model.addAttribute("cardid", result.getTeacher().getTeacher_cardid());
				model.addAttribute("msg", result.getMsg());
				return "index";
			}
		} else {
			model.addAttribute("readroomList", readroomService.findReadroomAll());
			model.addAttribute("msg", result.getMsg());
			return "WeAdmin/login";
		}
	}

	@RequestMapping("/logout")
	@ResponseBody
	public boolean logout(Model model, HttpServletRequest request, String cardid) {
		HttpSession session = request.getSession();
		boolean isOK = adminService.logout(cardid);
		if (isOK) {
			session.removeAttribute("teacher");
			session.removeAttribute("student");
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping("/jumpStudentLook")
	public String jumpStudentLook(Model model, String student_cardid) {
		Student student = studnetService.findStudentByStudent_cardid(student_cardid);
		model.addAttribute("student", student);
		return "WeAdmin/pages/student/studentLook";

	}

	@RequestMapping("/jumpTeacherLook")
	public String jumpTeacherLook(Model model, String teacher_cardid) {
		Teacher teacher = teacherService.findTeacherByTeacher_cardid(teacher_cardid);
		model.addAttribute("teacher", teacher);
		return "WeAdmin/pages/teacher/teacherLook";

	}
}
