package com.ysd.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ysd.service.StatisticsService;
import com.ysd.util.EchartsResult;
import com.ysd.util.EchartsResult2;
import com.ysd.util.Result;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {
	@Autowired
	private StatisticsService statisticsService;
	/**
	 *跳转页面
	 * @return
	 */
	@RequestMapping("/echarts1")
	public  String echarts1() {
		return "WeAdmin/pages/echarts/echarts1";
	}
	
	
	/**
	 *查询各个浏览室浏览量（饼状图）
	 * @return
	 */
	@RequestMapping("/selectReadRoomPeople")
	@ResponseBody
	public  EchartsResult selectReadRoomPeople() {
		return statisticsService.selectReadRoomPeople();
	}
	/**
	 *查询各个浏览室浏览量（饼状图）
	 * @return
	 */
	@RequestMapping("/selectReadRoomVolume")
	@ResponseBody
	public  EchartsResult selectReadRoomVolume() {
		return statisticsService.selectReadRoomVolume();
	}
	
	/**
	 *查询各个浏览室浏览量（柱状图）
	 * @return
	 */
	@RequestMapping("/selectReadRoomPeopleHistogram")
	@ResponseBody
	public  EchartsResult selectReadRoomPeopleHistogram() {
		return statisticsService.selectReadRoomPeopleHistogram();
	}
	
	/**
	 *查询各个浏览室男女人数比例List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecMalefemaleratioListHistogram")
	@ResponseBody
	public List<List<?>> selecMalefemaleratioListHistogram(EchartsResult2 echartsResult2){
		return statisticsService.selecMalefemaleratioListHistogram(echartsResult2);
	};
	
	/**
	 *查询各个浏览室男女浏览量比例List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecBrowseleratioListHistogram")
	@ResponseBody
	public List<List<?>> selecBrowseleratioListHistogram(EchartsResult2 echartsResult2){
		return statisticsService.selecBrowseleratioListHistogram(echartsResult2);
	};
	
	/**
	 *查询各个浏览室人数学历List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecBrowseEducationListHistogram")
	@ResponseBody
	public List<List<?>> selecBrowseEducationListHistogram(EchartsResult2 echartsResult2){
		return statisticsService.selecBrowseEducationListHistogram(echartsResult2);
	};
	
	/**
	 *查询各个浏览室人数学历List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecseleraEducationListHistogram")
	@ResponseBody
	public List<List<?>> selecseleraEducationListHistogram(EchartsResult2 echartsResult2){
		return statisticsService.selecseleraEducationListHistogram(echartsResult2);
	};
	
	/**
	 *查询各个浏览室人数院系List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecNumberofbrowsingroomsList")
	@ResponseBody
	public List<List<?>> selecNumberofbrowsingroomsList(EchartsResult2 echartsResult2){
		return statisticsService.selecNumberofbrowsingroomsList(echartsResult2);
	};
	
	/**
	 *查询各个浏览室浏览量院系List集合表示（柱状图）
	 * @return
	 */
	@RequestMapping("/selecseleraofbrowsingroomsList")
	@ResponseBody
	public List<List<?>> selecseleraofbrowsingroomsList(EchartsResult2 echartsResult2){
		return statisticsService.selecseleraofbrowsingroomsList(echartsResult2);
	};
	
}
