package com.ysd.controller;

import com.ysd.entity.Student;
import com.ysd.service.StudentService;
import com.ysd.util.ExcelUtils;
import com.ysd.util.PageNation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("student")
public class StudentController {
	@Autowired
	private StudentService studentService;

	/**
	 * 学生分页方法
	 * 
	 * @param pageNation
	 * @return
	 */
	@RequestMapping("findStudentPage")
	@ResponseBody
	public PageNation findStudentPage(PageNation pageNation) {
		return studentService.findStudentPage(pageNation);
	}

	@RequestMapping("studnetExport")
	public void studnetExport(HttpServletResponse response) {
		List<Student> studentList = studentService.findStudentAll();
		// 导出操作
		ExcelUtils.exportExcel(studentList, "学生信息导出", "学生信息", Student.class, "学生信息.xls", response);
	}

	/**
	 * 学生excel的导入方法
	 * @param file 前台上传的excel文件
	 * @return
	 */
	@RequestMapping("studnetImport")
	@ResponseBody
	public Map<String, Object> studnetImport(MultipartFile file) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			map.put("success", false);
			map.put("msg", "上传文件格式不正确");
			return map;
		}
		try {
			return studentService.studentImport(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", "上传失败");
			return map;
		}

	}
}
