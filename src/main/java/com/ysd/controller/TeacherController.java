package com.ysd.controller;

import com.ysd.entity.Teacher;
import com.ysd.service.TeacherService;
import com.ysd.util.ExcelUtils;
import com.ysd.util.PageNation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("teacher")
public class TeacherController {
	@Autowired
	private TeacherService teacherService;

	@RequestMapping("findTeacherPage")
	@ResponseBody
	public PageNation findTeacherPage(PageNation pageNation) {
		return teacherService.findTeacherPage(pageNation);
	}
	@RequestMapping("teacherExport")
	public void teacherExport(HttpServletResponse response) {
		List<Teacher> teacherList=teacherService.findTeacherAll();
		ExcelUtils.exportExcel(teacherList, "教师信息导出", "教师信息", Teacher.class, "教师信息.xls", response);
	}
	@RequestMapping("teacherImport")
	@ResponseBody
	public Map<String, Object> studnetImport(MultipartFile file) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		String fileName = file.getOriginalFilename();
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			map.put("success", false);
			map.put("msg", "上传文件格式不正确");
			return map;
		}
		try {
			return teacherService.teacherImport(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", "上传失败");
			return map;
		}

	}
}
