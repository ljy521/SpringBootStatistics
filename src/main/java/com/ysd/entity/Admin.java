package com.ysd.entity;

import lombok.Data;

@Data
public class Admin {
    private Integer admin_id;
    private String admin_username;
    private String admin_password;

}
