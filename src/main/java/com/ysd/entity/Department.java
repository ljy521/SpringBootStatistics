package com.ysd.entity;

import lombok.Data;

/**
 * 院系实体类
 */
@Data
public class Department {
    private Integer department_id;// 院系表id
    private String department_name;// 院系名称
    private String department_remark;// 备注
    private String department_exit1;// 预留字段1
    private Integer department_exit2;// 预留字段2
}
