package com.ysd.entity;

import lombok.Data;

/**
 * 阅览室实体类
 */
@Data
public class Readroom {
    private Integer readroom_id;// 阅览室id
    private String readroom_name;// 阅览室名称
    private String readroom_remark;// 备注
    private String readroom_exit1;// 预留字段1
    private Integer readroom_exit2;// 预留字段2
}
