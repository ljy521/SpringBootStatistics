package com.ysd.entity;

import lombok.Data;

/**
 * 科室表
 */
@Data
public class Section {
    private Integer section_id;// 科室表id
    private String section_name;// 科室名称
    private String section_remark;// 备注
    private String section_exit1;// 预留字段1
    private Integer section_exit2;// 预留字段2
}
