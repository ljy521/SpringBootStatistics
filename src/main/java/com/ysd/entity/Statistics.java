package com.ysd.entity;

import lombok.Data;

@Data
public class Statistics {
    private Integer statistics_id;// 科室表id
    private Integer statistics_readroomid;// 阅览室id(readroom表readroom_id)
    private Integer statistics_peoplenum;// 当天总浏览人数
    private Integer statistics_ageviews; // 当天总浏览量
    private String statistics_duration;// 当天总浏览时长
    private Integer statistics_maleageviews;// 男性当天浏览量
    private String statistics_maleduration;// 男性当天浏览时长
    private Integer statistics_malenum;// 男性当天浏览人数
    private String statistics_departmentageviews;// 系当天浏览量（数据格式：1,2,3）
    private String statistics_departmentduration;// 院系当天浏览时长（数据格式：1,2,3）
    private String statistics_departmentnum;// 院系当天浏览人数（数据格式：1,2,3）
    private String statistics_sectionageviews;// 科室当天浏览量（数据格式：1,2,3）
    private String statistics_sectionduration;// 科室当天浏览时长（数据格式：1,2,3）
    private String statistics_sectionnum;// 科室当天浏览人数（数据格式：1,2,3）
    private String statistics_educationageviews;// 学历当天浏览量（数据格式：1,2,3）
    private String statistics_educationduration;// 学历当天浏览时长（数据格式：1,2,3）
    private String statistics_educationnum;// 学历当天浏览人数（数据格式：1,2,3）
    private String statistics_day;// 统计日期
    private String statistics_remark;// 备注
    private String statistics_exit1;// 预留字段1
    private Integer statistics_exit2;// 预留字段2
    private Readroom readroom;//阅览室
}
