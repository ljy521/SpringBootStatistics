package com.ysd.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import java.util.List;

@Data
@ExcelTarget("20")
public class Student {
    private Integer student_id;// 学生表id
    @Excel(name = "学生卡号", orderNum = "0", width = 50)
    private String student_cardid;// 卡号
    @Excel(name = "姓名", orderNum = "1", width = 30)
    private String student_name;// 姓名
    @Excel(name = "性别", replace = { "男_1", "女_0" }, orderNum = "2", width = 30)
    private Integer student_sex;// 性别（1:男，0:女）
    @Excel(name = "学历", orderNum = "3", width = 25)
    private String student_education;// 学历
    @Excel(name = "专业名称", orderNum = "4", width = 25)
    private String student_profession;// 专业名称
    @Excel(name = "院系编号", orderNum = "5", width = 30)
    private Integer student_departmentid;// 院系编号（department表department_id）
    @Excel(name = "学生状态", orderNum = "6", width = 30)
    private Integer student_status;// 学生状态（0为未进入，0以上为阅览室表id）
    @Excel(name = "备注", orderNum = "7", width = 70)
    private String student_remark;// 备注
    private String student_exit1;// 预留字段1
    private Integer student_exit2;// 预留字段2
    private Department department;// 院系实体类
    private Readroom readroom;// 阅览室
    private String errorMsg;
    private String row;
    private List<Userlog> userlogList;
}
