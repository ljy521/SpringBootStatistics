package com.ysd.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.List;

@Data
public class Teacher {
    private Integer teacher_id;// 教师表id
    @Excel(name = "教师卡号", orderNum = "0", width = 50)
    private String teacher_cardid;// 卡号
    @Excel(name = "姓名", orderNum = "1", width = 30)
    private String teacher_name;// 姓名
    @Excel(name = "性别", replace = { "男_1", "女_0" }, orderNum = "2", width = 30)
    private Integer teacher_sex;// 性别（1:男，0:女)
    @Excel(name = "科室", orderNum = "3", width = 25)
    private Integer teacher_sectionid;// 科室编号（section表section_id）
    @Excel(name = "教师状态", orderNum = "4", width = 30)
    private Integer teacher_status;// 教师状态（0为未进入，0以上为阅览室表id）
    @Excel(name = "备注", orderNum = "5", width = 70)
    private String teacher_remark;// 备注
    private String teacher_exit1;// 预留字段1
    private Integer teacher_exit2;// 预留字段2
    private Section section;// 科室
    private Readroom readroom;// 阅览室id
    private String errorMsg;
    private String row;
    private List<Userlog> userlogList;
}
