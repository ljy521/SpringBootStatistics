package com.ysd.entity;

import lombok.Data;

@Data
public class Userlog {
    private Integer userlog_id;// 日志id
    private Integer userlog_readroomid;// 阅览室id(readroom表readroom_id)
    private String userlog_cardid;// 卡号
    private String userlog_intime;// 进入时间
    private String userlog_outtime;// 离开时间
    private Integer userlog_status;// 当前状态（1:在，0:不在）
    private String userlog_remark;// 备注
    private String userlog_exit1;// 预留字段1
    private Integer userlog_exit2;// 预留字段2
}
