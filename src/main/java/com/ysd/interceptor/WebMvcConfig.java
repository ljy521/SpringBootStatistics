package com.ysd.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	@Autowired
	private LoginInterceptor loginInterceptor;

	// 这个方法用来注册拦截器，我们自己写好的拦截器需要通过这里添加注册才能生效
	@Override
	public void addInterceptors(InterceptorRegistry interceptorRegistry) {
		// addPathPatterns("/**") 表示拦截所有的请求，
		// excludePathPatterns("/login", "/register") 表示除了登陆与注册之外，因为登陆注册不需要登陆也可以访问
		interceptorRegistry.addInterceptor(loginInterceptor).addPathPatterns("/**").excludePathPatterns("/admin/jumpLogin","/admin/jumpIndex","/admin/jumpAdminLogin","/admin/login","/css/**","/fonts/**","/images/**","/img/**","/js/**","/json/**","/layim/**","/layui/**","/layui-eleTree/**","/layui230/**","/mods/**","/404.html","/500.html","/favicon.ico");
		System.out.println("我来拦截规则了");
	}
	
//	配置静态资源文件夹,放行的东西
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry) {
		resourceHandlerRegistry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
		resourceHandlerRegistry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
		resourceHandlerRegistry.addResourceHandler("/static/").addResourceLocations("classpath:/static/**");
		resourceHandlerRegistry.addResourceHandler("/templates/").addResourceLocations("classpath:/templates/**");
	}
}