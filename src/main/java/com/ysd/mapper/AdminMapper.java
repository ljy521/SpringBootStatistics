package com.ysd.mapper;

import com.ysd.entity.Admin;

public interface AdminMapper {
	/**
	 * 根据用户名和密码寻找管理员
	 * @param admin
	 * @return
	 */
	public Admin findAdminByUsernameAndPassword(Admin admin);
}
