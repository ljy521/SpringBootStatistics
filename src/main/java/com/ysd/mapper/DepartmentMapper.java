package com.ysd.mapper;

import java.util.List;

import com.ysd.entity.Department;

public interface DepartmentMapper {
	/**
	 * 查找所有院系
	 * @return
	 */
	public List<Department> findDepartmentAll();
}
