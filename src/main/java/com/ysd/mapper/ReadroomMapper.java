package com.ysd.mapper;

import com.ysd.entity.Readroom;

import java.util.List;

public interface ReadroomMapper {
	/**
	 * 获取所有资源室
	 * @return
	 */
	public List<Readroom> findReadroomAll();
}
