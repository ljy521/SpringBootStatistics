package com.ysd.mapper;

import java.util.List;

import com.ysd.entity.Section;

public interface SectionMapper {
	/**
	 * 查找所有科室
	 * @return
	 */
	public List<Section> findSectionAll();
}
