package com.ysd.mapper;
import com.ysd.util.EchartsResult2;

import java.util.List;
import java.util.Map;

/**
 * 统计表
 * @return
 */
public interface StatisticsMapper {
	/**
	 *查询各个浏览室浏览人数Map集合表示（饼状图）
	 * @return
	 */
	public List<Map<String, Object>> selectReadRoomPeopleMap();
	/**
	 * 查询各个浏览室List集合表示（饼状图）
	 * @return
	 */
	public List<String> selectReadRoomNameList();
	
	/**
	 *查询各个浏览室浏览量Map集合表示（饼状图）
	 * @return
	 */
	public List<Map<String, Object>> selecBrowsevolumetMap();
	
	/**
	 * 查询各个浏览室人数List集合表示（柱状图）
	 * @return
	 */
	public List<String> selectReadRoomNameListHistogram();
	/**
	 *查询各个浏览室浏览量List集合表示（柱状图）
	 * @return
	 */
	public List<String> selecBrowsevolumetListHistogram();
	/**
	 *查询各个浏览室男女人数比例List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecMalefemaleratioListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室浏览量男女比例List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecBrowseleratioListHistogram(EchartsResult2 echartsResult2);
	
	
	/**
	 *查询各个浏览室人数学历List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecBrowseEducationListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室浏览量学历List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecseleraEducationListHistogram(EchartsResult2 echartsResult2);
	

	/**
	 *查询各个浏览室人数院室List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecNumberofbrowsingroomsList(EchartsResult2 echartsResult2);
	

	/**
	 *查询各个浏览室浏览量院室List集合表示（柱状图）
	 * @return
	 */
	public List<EchartsResult2> selecseleraofbrowsingroomsList(EchartsResult2 echartsResult2);
	
}
