package com.ysd.mapper;

import java.util.List;

import com.ysd.util.PageNation;
import com.ysd.entity.Student;

public interface StudentMapper {
	/**
	 * 查询所有学生
	 * 
	 * @return
	 */
	public List<Student> findStudentAll();

	/**
	 * 分页数据方法
	 * 
	 * @param pageNation
	 * @return
	 */
	public List<Student> findStudentPage(PageNation pageNation);

	/**
	 * 分页条数
	 * 
	 * @param pageNation
	 * @return
	 */
	public int findStudentCount(PageNation pageNation);

	/**
	 * 根据学生表的学号查找该学生信息
	 * 
	 * @param student_cardid
	 * @return
	 */
	public Student findStudentByStudent_cardid(String student_cardid);

	/**
	 * 修改学生的状态
	 * 
	 * @param student
	 * @return
	 */
	public int updateStudentStatus(Student student);

	/**
	 * 根据卡号查找该卡号的学生实际存在与否,用以辅助表格导入学生信息
	 * @param student_cardid
	 * @return
	 */
	public int findStudentIsTrueByStudent_cardid(String student_cardid);
	/**
	 * 添加学生方法,返回受影响行数
	 * @param studnet
	 * @return
	 */
	public int addStudent(Student studnet);
}
