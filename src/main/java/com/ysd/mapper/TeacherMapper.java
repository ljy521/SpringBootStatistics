package com.ysd.mapper;

import java.util.List;

import com.ysd.entity.Teacher;
import com.ysd.util.PageNation;

public interface TeacherMapper {
	/**
	 * 教师分页数据方法
	 * 
	 * @param pageNation
	 * @return
	 */
	public List<Teacher> findTeacherPage(PageNation pageNation);

	/**
	 * 教师分页的条数方法
	 * 
	 * @param pageNation
	 * @return
	 */
	public Integer findTeacherCount(PageNation pageNation);

	/**
	 * 根据教师工号查找该教师信息,查找该学生信息
	 * 
	 * @param teacher_cardid
	 * @return
	 */
	public Teacher findTeacherByTeacher_cardid(String teacher_cardid);

	/**
	 * 修改Teacher状态
	 * 
	 * @param teacher
	 * @return
	 */
	public int updateTeacherStatus(Teacher teacher);

	/**
	 * 根据卡号查找该卡号的教师实际存在与否,用以辅助表格导入教师信息
	 * 
	 * @param teacher_cardid
	 * @return
	 */
	public int findTeacherIsTrueByTeacher_cardid(String teacher_cardid);

	/**
	 * 添加教师方法,返回受影响行数
	 * 
	 * @param teacher
	 * @return
	 */
	public int addTeacher(Teacher teacher);

	/**
	 * 查找所有教师
	 * @return
	 */
	public List<Teacher> findTeacherAll();
}
