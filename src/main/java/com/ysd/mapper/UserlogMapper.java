package com.ysd.mapper;

import java.util.List;

import com.ysd.entity.Userlog;

public interface UserlogMapper {
	/**
	 * 查找所有用户日志表
	 * 
	 * @return
	 */
	public List<Userlog> findUserlogAll();

	/**
	 * 添加用户日志表
	 * 
	 * @param userlog
	 * @return
	 */
	public int addUserlog(Userlog userlog);

	/**
	 * 修改用户日志表
	 * 
	 * @param userlog
	 * @return
	 */
	public int updateUserlog(Userlog userlog);

	/**
	 * 根据用户日志表id，查找数据
	 * 
	 * @param userlog_id
	 * @return
	 */
	public Userlog getUserlogById(Integer userlog_id);

	/**
	 * 根据卡号拿到该卡号的最后一条日志记录
	 * @param userlog_cardid
	 * @return
	 */
	public Userlog getUserlogLastOneByCardid(String userlog_cardid);
}
