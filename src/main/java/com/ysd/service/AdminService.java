package com.ysd.service;

import com.ysd.entity.Admin;
import com.ysd.util.Result;

public interface AdminService {
	/**
	 * 传入前台卡号和资源室id，通过卡号判断卡号是否存在，进行登录。
	 * 
	 * @param cardid
	 * @param readroomid
	 * @return
	 */
	public Result login(String cardid, Integer readroomid);

	/**
	 * 根据卡号进行签退
	 * 
	 * @param cardid
	 * @return
	 */
	public boolean logout(String cardid);

	/**
	 * 根据管理员用户和密码查找是否有该管理员
	 * @param admin
	 * @return
	 */
	public Result adminLogin(Admin admin);
}
