package com.ysd.service;

import java.util.List;

import com.ysd.entity.Department;

public interface DepartmentService {
	/**
	 * 查找所有院系
	 * @return
	 */
	public List<Department> findDepartmentAll();
}
