package com.ysd.service;

import com.ysd.entity.Readroom;

import java.util.List;

public interface ReadroomService {
	/**
	 * 查找所有资源室
	 * 
	 * @return
	 */
	public List<Readroom> findReadroomAll();
}
