package com.ysd.service;

import java.util.List;

import com.ysd.entity.Section;

public interface SectionService {
	/**
	 * 查找所有科室
	 * @return
	 */
	public List<Section> findSectionAll();
}
