package com.ysd.service;

import java.util.List;

import com.ysd.util.EchartsResult;
import com.ysd.util.EchartsResult2;
import com.ysd.util.Result;

public interface StatisticsService {
	/**
	 *查询各个浏览室浏览人数（饼状图）
	 * @return
	 */
	public EchartsResult selectReadRoomPeople();
	
	/**
	 *查询各个浏览室浏览量（饼状图）
	 * @return
	 */
	public EchartsResult selectReadRoomVolume();
	
	/**
	 *查询各个浏览室浏览人数（柱状图）
	 * @return
	 */
	public EchartsResult selectReadRoomPeopleHistogram();
	/**
	 *查询各个浏览室浏览量（柱状图）
	 * @return
	 */
	public EchartsResult selectReadRoomVolumeHistogram();
	/**
	 *查询各个浏览室男女人数比例List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecMalefemaleratioListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室男女浏览量比例List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecBrowseleratioListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室人数学历List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecBrowseEducationListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室浏览量学历List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecseleraEducationListHistogram(EchartsResult2 echartsResult2);
	
	/**
	 *查询各个浏览室人数院室List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecNumberofbrowsingroomsList(EchartsResult2 echartsResult2);
	

	/**
	 *查询各个浏览室浏览量院室List集合表示（柱状图）
	 * @return
	 */
	public List<List<?>> selecseleraofbrowsingroomsList(EchartsResult2 echartsResult2);
}
