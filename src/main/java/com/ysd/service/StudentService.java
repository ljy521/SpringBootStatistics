package com.ysd.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.ysd.util.PageNation;
import com.ysd.util.Result;
import com.ysd.entity.Student;

public interface StudentService {
	/**
	 * 查询所有学生数据
	 * 
	 * @return
	 */
	public List<Student> findStudentAll();

	/**
	 * 学生的分页方法
	 * 
	 * @param pageNation
	 * @return
	 */
	public PageNation findStudentPage(PageNation pageNation);

	/**
	 * 学生导入excel表格的方法
	 * @param fileName
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public Map<String,Object> studentImport(MultipartFile file) throws IOException;
	/**
	 * 根据卡号查找该学生信息
	 * @param student_cardid
	 * @return
	 */
	public Student findStudentByStudent_cardid(String student_cardid);
}
