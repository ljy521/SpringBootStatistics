package com.ysd.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.ysd.entity.Teacher;
import com.ysd.util.PageNation;
import com.ysd.util.Result;

public interface TeacherService {
	/**
	 * 教师分页方法
	 * 
	 * @param page
	 * @return
	 */
	public PageNation findTeacherPage(PageNation page);

	/**
	 * 教师导入excel表格的方法
	 * 
	 * @param fileName
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public Map<String, Object> teacherImport(MultipartFile file) throws IOException;

	/**
	 * 查找所有老师
	 * @return
	 */
	public List<Teacher> findTeacherAll();
	/**
	 * 根据工号查找该教师
	 * @param teacher_cardid
	 * @return
	 */
	public Teacher findTeacherByTeacher_cardid(String teacher_cardid);
}
