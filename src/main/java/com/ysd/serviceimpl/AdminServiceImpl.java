package com.ysd.serviceimpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ysd.entity.Admin;
import com.ysd.entity.Student;
import com.ysd.entity.Teacher;
import com.ysd.entity.Userlog;
import com.ysd.mapper.AdminMapper;
import com.ysd.mapper.StudentMapper;
import com.ysd.mapper.TeacherMapper;
import com.ysd.mapper.UserlogMapper;
import com.ysd.service.AdminService;
import com.ysd.util.Result;

@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	private StudentMapper studentMapper;
	@Autowired
	private TeacherMapper teacherMapper;
	@Autowired
	private UserlogMapper userlogMapper;
	@Autowired
	private AdminMapper adminMapper;

	@Override
	public Result login(String cardid, Integer readroomid) {
		// TODO Auto-generated method stub
		Result result = new Result();
		Student student = studentMapper.findStudentByStudent_cardid(cardid);
		Teacher teacher = teacherMapper.findTeacherByTeacher_cardid(cardid);
		if (student != null) {// 说明卡号存在
			if (student.getStudent_status() > 0) {// 如果大于0意味着之前去过资源教室
				Integer num = student.getStudent_status();
				// 上次逃的阅览室是不是这次想要进入的阅览室？
				if (num == readroomid) {// 如果是直接进入
					result.setStudent(student);
					result.setSuccess(true);
					result.setMsg("欢迎来到" + student.getReadroom().getReadroom_name() + "下一次请通过正常流程进入退出");
					return result;
				} else {// 不一样，让他先去上一个阅览室进行签退
					result.setSuccess(false);
					result.setMsg("请去" + student.getReadroom().getReadroom_name() + "进行签退");
					return result;
				}
			} // 说明是之前正常退出,现在正常进入，修改状态，添加记录
			else {
				student.setStudent_status(readroomid);
				studentMapper.updateStudentStatus(student);
				Userlog userlog = new Userlog();
				userlog.setUserlog_readroomid(readroomid);
				userlog.setUserlog_cardid(cardid);
				userlog.setUserlog_intime(
						LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm")));
				userlog.setUserlog_status(1);
				userlogMapper.addUserlog(userlog);
				Student newStudent = studentMapper.findStudentByStudent_cardid(cardid);
				result.setStudent(newStudent);
				result.setSuccess(true);
				result.setMsg("欢迎来到" + newStudent.getReadroom().getReadroom_name());
				return result;
			}
		}
		if (teacher != null) {
			if (teacher.getTeacher_status() > 0) {// 如果大于0意味着之前去过资源教室
				Integer num = teacher.getTeacher_status();
				// 上次逃的阅览室是不是这次想要进入的阅览室？
				if (num == readroomid) {// 如果是直接进入
					result.setTeacher(teacher);
					result.setSuccess(true);
					result.setMsg("欢迎来到" + teacher.getReadroom().getReadroom_name() + "下一次请通过正常流程进入退出");
					return result;
				} else {// 不一样，让他先去上一个阅览室进行签退
					result.setSuccess(false);
					result.setMsg("请去" + teacher.getReadroom().getReadroom_name() + "进行签退");
					return result;
				}
			} // 说明是之前正常退出,现在正常进入，修改状态，添加记录
			else {
				teacher.setTeacher_status(readroomid);
				teacherMapper.updateTeacherStatus(teacher);
				Userlog userlog = new Userlog();
				userlog.setUserlog_readroomid(readroomid);
				userlog.setUserlog_cardid(cardid);
				userlog.setUserlog_intime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm")));
				userlog.setUserlog_status(1);
				userlogMapper.addUserlog(userlog);
				Teacher newTeacher = teacherMapper.findTeacherByTeacher_cardid(cardid);
				result.setTeacher(newTeacher);
				result.setSuccess(true);
				System.out.println(teacher+"-----------------------------");
				result.setMsg("欢迎来到" + newTeacher.getReadroom().getReadroom_name());
				return result;
			}
		} else {
			result.setSuccess(false);
			result.setMsg("无此卡号");
			return result;
		}

	}

	@Override
	public boolean logout(String cardid) {
		// TODO Auto-generated method stub
		Student student = studentMapper.findStudentByStudent_cardid(cardid);
		Teacher teacher = teacherMapper.findTeacherByTeacher_cardid(cardid);
		//根据卡号拿到该卡号的最后一条日志记录，然后进行离开修改
		Userlog userlog = userlogMapper.getUserlogLastOneByCardid(cardid);
		String nowTime=LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm"));
		userlog.setUserlog_outtime(nowTime);
		userlog.setUserlog_status(0);
		System.out.println(userlog);
		userlogMapper.updateUserlog(userlog);
		if(student!=null) {
			student.setStudent_status(0);
			studentMapper.updateStudentStatus(student);
			return true;
		}
		if(teacher!=null) {
			teacher.setTeacher_status(0);
			teacherMapper.updateTeacherStatus(teacher);
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public Result adminLogin(Admin admin) {
		// TODO Auto-generated method stub
		Result result = new Result();
		Admin newAdmin=adminMapper.findAdminByUsernameAndPassword(admin);
		if(newAdmin!=null) {
			result.setSuccess(true);
			result.setAdmin(newAdmin);
			result.setMsg("欢迎进入");
		}
		else {
			result.setSuccess(false);
			result.setMsg("用户名或密码错误");
		}
		return result;
	}

}
