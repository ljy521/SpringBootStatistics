package com.ysd.serviceimpl;

import com.ysd.entity.Department;
import com.ysd.mapper.DepartmentMapper;
import com.ysd.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	@Autowired
	private DepartmentMapper departmentMapper;

	@Override
	public List<Department> findDepartmentAll() {
		// TODO Auto-generated method stub
		return departmentMapper.findDepartmentAll();
	}

}
