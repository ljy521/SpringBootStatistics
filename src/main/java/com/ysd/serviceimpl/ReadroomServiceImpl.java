package com.ysd.serviceimpl;

import com.ysd.entity.Readroom;
import com.ysd.mapper.ReadroomMapper;
import com.ysd.service.ReadroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReadroomServiceImpl implements ReadroomService {
	@Autowired
	private ReadroomMapper readroomMapper;

	@Override
	public List<Readroom> findReadroomAll() {
		// TODO Auto-generated method stub
		return readroomMapper.findReadroomAll();
	}

}
