package com.ysd.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ysd.entity.Section;
import com.ysd.mapper.SectionMapper;
import com.ysd.service.SectionService;

@Service
public class SectionServiceImpl implements SectionService {
	@Autowired
	private SectionMapper sectionMapper;

	@Override
	public List<Section> findSectionAll() {
		// TODO Auto-generated method stub
		return sectionMapper.findSectionAll();
	}

}
