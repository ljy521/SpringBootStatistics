package com.ysd.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ysd.mapper.StatisticsMapper;
import com.ysd.service.StatisticsService;
import com.ysd.util.Convert;
import com.ysd.util.EchartsResult;
import com.ysd.util.EchartsResult2;
import com.ysd.util.Result;


@Service
public class StatisticsServiceImpl implements StatisticsService {
	@Autowired
	private StatisticsMapper statisticsMapper;
	/**
	 *查询各个浏览室浏览人数（饼状图）
	 * @return
	 */
	@Override
	public EchartsResult selectReadRoomPeople() {
		// TODO Auto-generated method stub	
		EchartsResult echartsResult = new EchartsResult();
		echartsResult.setEchartsResult_selectReadRoomNameList(statisticsMapper.selectReadRoomNameList());
		echartsResult.setEchartsResult_selectReadRoomPeopleMap(statisticsMapper.selectReadRoomPeopleMap());
		return echartsResult;
	}
	/**
	 *查询各个浏览室浏览量（饼状图）
	 * @return
	 */
	@Override
	public EchartsResult selectReadRoomVolume() {
		// TODO Auto-generated method stub
		EchartsResult echartsResult = new EchartsResult();
		echartsResult.setEchartsResult_selectReadRoomNameList(statisticsMapper.selectReadRoomNameList());
		echartsResult.setEchartsResult_selecBrowsevolumetMap(statisticsMapper.selecBrowsevolumetMap());
		return echartsResult;
	}
	/**
	 *查询各个浏览室浏览人数（柱状图）
	 * @return
	 */
	@Override
	public EchartsResult selectReadRoomPeopleHistogram() {
		EchartsResult echartsResult = new EchartsResult();
		echartsResult.setEchartsResult_selectReadRoomNameList(statisticsMapper.selectReadRoomNameList());
		echartsResult.setEchartsResult_selectReadRoomNameListHistogram(statisticsMapper.selectReadRoomNameListHistogram());
		return echartsResult;
	}
	/**
	 *查询各个浏览室浏览量（柱状图）
	 * @return
	 */
	@Override
	public EchartsResult selectReadRoomVolumeHistogram() {
		EchartsResult echartsResult = new EchartsResult();
		echartsResult.setEchartsResult_selectReadRoomNameList(statisticsMapper.selectReadRoomNameList());
		echartsResult.setEchartsResult_selecBrowsevolumetListHistogram(statisticsMapper.selecBrowsevolumetListHistogram());
		return echartsResult;
	}
	/**
	 *查询各个浏览室男女人数比例List集合表示（柱状图）
	 * @return
	 */
	@Override
	public List<List<?>> selecMalefemaleratioListHistogram(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
		List<List<?>> list = new ArrayList<>();
		
		List<EchartsResult2> list2 = statisticsMapper.selecMalefemaleratioListHistogram(echartsResult2);
		for(int i = 0 ; i < list2.size();i++) {
			list.add(Convert.ConvertObjToList(list2.get(i)));
		}
		return list;
	}
	
	/**
	 *查询各个浏览室男女浏览量人数比例List集合表示（柱状图）
	 * @return
	 */
	@Override
	public List<List<?>> selecBrowseleratioListHistogram(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
        List<List<?>> list2 = new ArrayList<>();
		List<EchartsResult2> list3 = statisticsMapper.selecBrowseleratioListHistogram(echartsResult2);
		for(int i = 0 ; i < list3.size();i++) {
			list2.add(Convert.ConvertObjToList(list3.get(i)));
		}
		return list2;
	}
	/**
	 *查询各个浏览室人数学历List集合表示（柱状图）
	 * @return
	 */
	@Override
	public List<List<?>> selecBrowseEducationListHistogram(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
        List<List<?>> list2 = new ArrayList<>();
		List<EchartsResult2> list3 = statisticsMapper.selecBrowseEducationListHistogram(echartsResult2);
		for(int i = 0 ; i < list3.size();i++) {
			list2.add(Convert.ConvertObjToList(list3.get(i)));
		}
		return list2;
	}
	/**
	 *查询各个浏览室浏览量学历List集合表示（柱状图）
	 * @return
	 */
	@Override
	public List<List<?>> selecseleraEducationListHistogram(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
        List<List<?>> list2 = new ArrayList<>();
		List<EchartsResult2> list3 = statisticsMapper.selecseleraEducationListHistogram(echartsResult2);
		for(int i = 0 ; i < list3.size();i++) {
			list2.add(Convert.ConvertObjToList(list3.get(i)));
		}
		return list2;
	}
	@Override
	public List<List<?>> selecNumberofbrowsingroomsList(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
        List<List<?>> list2 = new ArrayList<>();
		List<EchartsResult2> list3 = statisticsMapper.selecNumberofbrowsingroomsList(echartsResult2);
		for(int i = 0 ; i < list3.size();i++) {
			list2.add(Convert.ConvertObjToList(list3.get(i)));
		}
		return list2;
	}
	@Override
	public List<List<?>> selecseleraofbrowsingroomsList(EchartsResult2 echartsResult2) {
		// TODO Auto-generated method stub
        List<List<?>> list2 = new ArrayList<>();
		List<EchartsResult2> list3 = statisticsMapper.selecseleraofbrowsingroomsList(echartsResult2);
		for(int i = 0 ; i < list3.size();i++) {
			list2.add(Convert.ConvertObjToList(list3.get(i)));
		}
		return list2;
	}
	

	
	
}
