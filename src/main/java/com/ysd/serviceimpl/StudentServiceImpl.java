package com.ysd.serviceimpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.ysd.entity.Student;
import com.ysd.mapper.StudentMapper;
import com.ysd.service.StudentService;
import com.ysd.util.PageNation;
import com.ysd.util.Result;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentMapper studentMapper;

	@Override
	public List<Student> findStudentAll() {
		// TODO Auto-generated method stub
		return studentMapper.findStudentAll();
	}

	@Override
	public PageNation findStudentPage(PageNation pageNation) {
		// TODO Auto-generated method stub
		PageNation page = new PageNation();
		page.setData(studentMapper.findStudentPage(pageNation));
		page.setCount(studentMapper.findStudentCount(pageNation));
		page.setMsg("危险");
		page.setCode(0);
		return page;
	}

//	@Override
//	public Result StudentImport(String fileName, MultipartFile file) throws IOException {
//		// TODO Auto-generated method stub
//		Result result = new Result();
//		List<Student> studentList = new ArrayList<>();
//		List<Student> list = new ArrayList<>();
//		boolean notNull = false;
//		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
//			result = new Result();
//			result.setSuccess(false);
//			result.setIsLockout(2);
//			result.setMsg("上传文件格式不正确");
//			return result;
//		}
//		boolean isExcel2003 = true;
//		if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
//			isExcel2003 = false;
//		}
//		InputStream is = file.getInputStream();
//		Workbook wb = null;
//		if (isExcel2003) {
//			wb = new HSSFWorkbook(is);
//		} else {
//			wb = new XSSFWorkbook(is);
//		}
//		Sheet sheet = wb.getSheetAt(0);
//		if (sheet != null) {
//			notNull = true;
//		}
//		Student student = null;
//		for (int r = 2; r <= sheet.getLastRowNum(); r++) {// r = 2 表示从第三行开始循环 如果你的第三行开始是数据
//			Row row = sheet.getRow(r);// 通过sheet表单对象得到 行对象
//			if (row == null) {
//				continue;
//			}
//			student = new Student();
//			try {
//				if (row.getCell(0).getCellType() != 1) {// 循环时，得到每一行的单元格进行判断
//					result = new Result();
//					result.setSuccess(false);
//					result.setIsLockout(2);
//					result.setMsg("导入失败(第" + (r + 1) + "行,请设为文本格式)");
//					return result;
//				}
//				student.setStudent_cardid(row.getCell(0).getStringCellValue());
//
//				row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第二个单元格的值
//				student.setStudent_name(row.getCell(1).getStringCellValue());
//				row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第3个单元格的值
//				String sex = row.getCell(2).getStringCellValue();
//				System.out.println(sex);
//				if (sex.equals("男") || sex.equals("女") || sex.equals("男生") || sex.equals("女生")) {
//					if (sex.equals("男") || sex.equals("男生")) {
//						student.setStudent_sex(1);
//					} else {
//						System.out.println(1);
//						student.setStudent_sex(0);
//					}
//				} else {
//					try {
//						if (Integer.parseInt(sex) > 1) {
//							result = new Result();
//							result.setSuccess(false);
//							result.setIsLockout(2);
//							result.setMsg("导入失败(第" + (r + 1) + "行,性别字段不符合要求！)");
//							return result;
//						}
//						student.setStudent_sex(Integer.parseInt(sex));
//
//					} catch (Exception e) {
//						// TODO: handle exception
//						result = new Result();
//						result.setSuccess(false);
//						result.setIsLockout(2);
//						result.setMsg("导入失败(第" + (r + 1) + "行,性别字段不符合要求！)");
//						return result;
//					}
//				}
//
//				row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第4个单元格的值
//				student.setStudent_profession(row.getCell(3).getStringCellValue());
//				row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第5个单元格的值
//				student.setStudent_education(row.getCell(4).getStringCellValue());
//				row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第6个单元格的值
//				student.setStudent_departmentid(Integer.parseInt(row.getCell(5).getStringCellValue()));
//
//				row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第7个单元格的值
//				student.setStudent_status(Integer.parseInt(row.getCell(6).getStringCellValue()));
//				row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第8个单元格的值
//				student.setStudent_remark(row.getCell(7).getStringCellValue());
//			} catch (NullPointerException e) {
//				// TODO: handle exception
//				result = new Result();
//				result.setSuccess(false);
//				result.setIsLockout(2);
//				result.setMsg("导入失败,工作表中数据不可不填数据，空请填入null");
//				return result;
//
//			}
//			if (!student.getStudent_cardid().substring(0, 1).equals("S")
//					|| student.getStudent_cardid().length() != 10) {
//				result = new Result();
//				result.setSuccess(false);
//				result.setIsLockout(2);
//				result.setMsg("导入失败(第" + (r + 1) + "行,卡号不符合规范！)");
//				return result;
//			}
//
//			if (student.getStudent_cardid().length() != 8) {
//				result = new Result();
//				result.setSuccess(false);
//				result.setIsLockout(2);
//				result.setMsg("导入失败(第" + (r + 1) + "行,学号不符合规范！)");
//				return result;
////				stu.setExit("第" + (r + 1) + "行");
////				stu.setExit2("学号不符合规范！");
////				list.add(stu);
////				continue;
//			}
//
//			studentList.add(student);
//		}
//		return result;
//	}
	@Override
	public Map<String, Object> studentImport(MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		boolean notNull = false;
		List<Student> teaList = new ArrayList<>();
		List<Student> list = new ArrayList<Student>();
		int num = 0;
		String fileName = file.getOriginalFilename();
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			map.put("success", false);
			map.put("msg", "上传文件格式不正确");
			return map;
		}
		boolean isExcel2003 = true;
		if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
			isExcel2003 = false;
		}
		InputStream is = file.getInputStream();
		Workbook wb = null;
		if (isExcel2003) {
			wb = new HSSFWorkbook(is);
		} else {
			wb = new XSSFWorkbook(is);
		}
		Sheet sheet = wb.getSheetAt(0);
		if (sheet != null) {
			notNull = true;
		}
		Student stu;
		for (int r = 2; r <= sheet.getLastRowNum(); r++) {// r = 2 表示从第三行开始循环 如果你的第三行开始是数据
			Row row = sheet.getRow(r);// 通过sheet表单对象得到 行对象
			Boolean isTea = true;
			if (row == null) {
				map.put("success", false);
				map.put("msg", "导入失败(第" + (r + 1) + "行,无数据)");
				return map;
			}

			stu = new Student();
			stu.setErrorMsg("");
			try {

				if (row.getCell(0).getCellType() != 1) {// 循环时，得到每一行的单元格进行判断
					map.put("success", false);
					map.put("msg", "导入失败(第" + (r + 1) + "行,请设为文本格式)");
					return map;
				}
				row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第1个单元格的值
				row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第2个单元格的值
				row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第3个单元格的值
				row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第4个单元格的值
				row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第5个单元格的值
				row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第6个单元格的值
				row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第7个单元格的值
				row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第8个单元格的值
				Cell row1 = (Cell) row.getCell(0);// 卡号
				Cell row2 = (Cell) row.getCell(1);// 姓名
				Cell row3 = (Cell) row.getCell(2);// 性别（1:男，0:女）
				Cell row4 = (Cell) row.getCell(3);// 专业名称
				Cell row5 = (Cell) row.getCell(4);// 学历
				Cell row6 = (Cell) row.getCell(5);// 院系编号
				Cell row7 = (Cell) row.getCell(6);// 学生状态
				Cell row8 = (Cell) row.getCell(7);// 备注
				stu.setStudent_cardid(row1.getStringCellValue());
				stu.setStudent_name(row2.getStringCellValue());
				try {
					stu.setStudent_sex(Integer.parseInt(row3.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "性别不能为空");
					stu.setStudent_sex(5);
					isTea = false;
				}
				stu.setStudent_profession(row4.getStringCellValue());
				stu.setStudent_education(row5.getStringCellValue());
				try {
					stu.setStudent_departmentid(Integer.parseInt(row6.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "-系别编号不能为空！");
					stu.setStudent_departmentid(0);
					isTea = false;
				}

				try {
					stu.setStudent_status(Integer.parseInt(row7.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "-状态不能为空！");
					stu.setStudent_status(0);
					isTea = false;
				}
				stu.setStudent_remark(row8.getStringCellValue());
				if (stu.getStudent_sex() > 1) {
					stu.setErrorMsg(stu.getErrorMsg() + "-性别字段不符合要求！");
					isTea = false;
				}
				if (stu.getStudent_cardid() == null || stu.getStudent_cardid().equals("")) {
					stu.setErrorMsg(stu.getErrorMsg() + "卡号不能为空！");
					isTea = false;
				}
				if (stu.getStudent_name() == null || stu.getStudent_name().equals("")) {
					stu.setErrorMsg(stu.getErrorMsg() + "-姓名不能为空！");
					isTea = false;
				}

				if (!isTea) {
					stu.setRow("第" + (r + 1) + "行");
					stu.setErrorMsg("0为数值类型占位符！" + stu.getErrorMsg());
					list.add(stu);
					continue;
				}

			} catch (NullPointerException e) {
				// TODO: handle exception
				map.put("success", false);
				map.put("msg", "导入失败(第" + (r + 1) + "行,工作表中数据不可不填数据，空请填入null)");
				return map;
			}
			if (stu.getStudent_cardid().indexOf("S") != 0 || stu.getStudent_cardid().length() != 9) {
				stu.setRow("第" + (r + 1) + "行");
				stu.setErrorMsg("卡号不符合规范！");
				list.add(stu);
				continue;
			}

			int count = studentMapper.findStudentIsTrueByStudent_cardid(stu.getStudent_cardid());
			if (count == 0) {
				num = studentMapper.addStudent(stu);
				continue;
			} else {
				stu.setRow("第" + (r + 1) + "行");
				stu.setErrorMsg("卡号出现重复！");
				list.add(stu);
				continue;
			}
		}
		map.put("errorList", list);
		if (list.toString().equals("[]")) {
			map.put("success", true);
			map.put("msg", "全部添加成功");
		} else if (num > 0 && list.size() > 0) {
			map.put("success", true);
			map.put("msg", "符合规范数据添加成功,错误数据返回！！");
		} else {
			map.put("success", false);
			map.put("msg", "没有可用数据,返回全部数据！！");
		}
		return map;
	}

	@Override
	public Student findStudentByStudent_cardid(String student_cardid) {
		// TODO Auto-generated method stub
		return studentMapper.findStudentByStudent_cardid(student_cardid);
	}
}
