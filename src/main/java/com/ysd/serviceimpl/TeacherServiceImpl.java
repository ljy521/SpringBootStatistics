package com.ysd.serviceimpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ysd.entity.Student;
import com.ysd.entity.Teacher;
import com.ysd.mapper.TeacherMapper;
import com.ysd.service.TeacherService;
import com.ysd.util.PageNation;
import com.ysd.util.Result;

@Service
public class TeacherServiceImpl implements TeacherService {
	@Autowired
	private TeacherMapper teacherMapper;

	@Override
	public PageNation findTeacherPage(PageNation pageNation) {
		PageNation page = new PageNation();
		page.setData(teacherMapper.findTeacherPage(pageNation));
		page.setMsg("");
		page.setCode(0);
		page.setCount(teacherMapper.findTeacherCount(pageNation));
		return page;
	}

	@Override
	public Map<String, Object> teacherImport(MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		boolean notNull = false;
		List<Teacher> teaList = new ArrayList<>();
		List<Teacher> list = new ArrayList<Teacher>();
		int num = 0;
		String fileName = file.getOriginalFilename();
		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			map.put("success", false);
			map.put("msg", "上传文件格式不正确");
			return map;
		}
		boolean isExcel2003 = true;
		if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
			isExcel2003 = false;
		}
		InputStream is = file.getInputStream();
		Workbook wb = null;
		if (isExcel2003) {
			wb = new HSSFWorkbook(is);
		} else {
			wb = new XSSFWorkbook(is);
		}
		Sheet sheet = wb.getSheetAt(0);
		if (sheet != null) {
			notNull = true;
		}
		Teacher stu;
		for (int r = 2; r <= sheet.getLastRowNum(); r++) {// r = 2 表示从第三行开始循环 如果你的第三行开始是数据
			Row row = sheet.getRow(r);// 通过sheet表单对象得到 行对象
			Boolean isTea = true;
			if (row == null) {
				map.put("success", false);
				map.put("msg", "导入失败(第" + (r + 1) + "行,无数据)");
				return map;
			}

			stu = new Teacher();
			stu.setErrorMsg("");
			try {

				if (row.getCell(0).getCellType() != 1) {// 循环时，得到每一行的单元格进行判断
					map.put("success", false);
					map.put("msg", "导入失败(第" + (r + 1) + "行,请设为文本格式)");
					return map;
				}
				row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第1个单元格的值
				row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第2个单元格的值
				row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第3个单元格的值
				row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第4个单元格的值
				row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第5个单元格的值
				row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);// 得到每一行的 第6个单元格的值
				Cell row1 = (Cell) row.getCell(0);// 卡号
				Cell row2 = (Cell) row.getCell(1);// 姓名
				Cell row3 = (Cell) row.getCell(2);// 性别（1:男，0:女）
				Cell row4 = (Cell) row.getCell(3);// 科室编号
				Cell row5 = (Cell) row.getCell(4);// 教师状态
				Cell row6 = (Cell) row.getCell(5);// 备注
				stu.setTeacher_cardid(row1.getStringCellValue());
				stu.setTeacher_name(row2.getStringCellValue());
				try {
					stu.setTeacher_sex(Integer.parseInt(row3.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "性别不能为空");
					stu.setTeacher_sex(5);
					isTea = false;
				}
				try {
					stu.setTeacher_sectionid(Integer.parseInt(row4.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "-科室编号不能为空！");
					stu.setTeacher_sectionid(0);
					isTea = false;
				}

				try {
					stu.setTeacher_status(Integer.parseInt(row5.getStringCellValue()));
				} catch (Exception e) {
					// TODO: handle exception
					stu.setErrorMsg(stu.getErrorMsg() + "-状态不能为空！");
					stu.setTeacher_status(0);
					isTea = false;
				}
				stu.setTeacher_remark(row6.getStringCellValue());
				if (stu.getTeacher_sex() > 1) {
					stu.setErrorMsg(stu.getErrorMsg() + "-性别字段不符合要求！");
					isTea = false;
				}
				if (stu.getTeacher_cardid() == null || stu.getTeacher_cardid().equals("")) {
					stu.setErrorMsg(stu.getErrorMsg() + "卡号不能为空！");
					isTea = false;
				}
				if (stu.getTeacher_name() == null || stu.getTeacher_name().equals("")) {
					stu.setErrorMsg(stu.getErrorMsg() + "-姓名不能为空！");
					isTea = false;
				}

				if (!isTea) {
					stu.setRow("第" + (r + 1) + "行");
					stu.setErrorMsg("0为数值类型占位符！" + stu.getErrorMsg());
					list.add(stu);
					continue;
				}

			} catch (NullPointerException e) {
				// TODO: handle exception
				map.put("success", false);
				map.put("msg", "导入失败(第" + (r + 1) + "行,工作表中数据不可不填数据，空请填入null)");
				return map;
			}
			if (stu.getTeacher_cardid().indexOf("T") != 0 || stu.getTeacher_cardid().length() != 9) {
				stu.setRow("第" + (r + 1) + "行");
				stu.setErrorMsg("卡号不符合规范！");
				list.add(stu);
				continue;
			}

			int count = teacherMapper.findTeacherIsTrueByTeacher_cardid(stu.getTeacher_cardid());
			if (count == 0) {
				num = teacherMapper.addTeacher(stu);
				continue;
			} else {
				stu.setRow("第" + (r + 1) + "行");
				stu.setErrorMsg("卡号出现重复！");
				list.add(stu);
				continue;
			}
		}
		map.put("errorList", list);
		if (list.toString().equals("[]")) {
			map.put("success", true);
			map.put("msg", "全部添加成功");
		} else if (num > 0 && list.size() > 0) {
			map.put("success", true);
			map.put("msg", "符合规范数据添加成功,错误数据返回！！");
		} else {
			map.put("success", false);
			map.put("msg", "没有可用数据,返回全部数据！！");
		}
		return map;
	}

	@Override
	public List<Teacher> findTeacherAll() {
		// TODO Auto-generated method stub
		return teacherMapper.findTeacherAll();
	}

	@Override
	public Teacher findTeacherByTeacher_cardid(String teacher_cardid) {
		// TODO Auto-generated method stub
		return teacherMapper.findTeacherByTeacher_cardid(teacher_cardid);
	}

}
