package com.ysd.util;

import java.util.List;
import java.util.Map;
/**
 * 首页图表辅助类
 * @return
 */
public class EchartsResult {
	/**
	 * 查询各个浏览室List集合表示（饼状图）
	 * @return
	 */
	private List<String> EchartsResult_selectReadRoomNameList;
	/**
	 *查询各个浏览室浏览人数Map集合表示（饼状图）
	 * @return
	 */
	private List<Map<String, Object>> EchartsResult_selectReadRoomPeopleMap;
	
	/**
	 *查询各个浏览室浏览量Map集合表示（饼状图）
	 * @return
	 */
	private List<Map<String, Object>> EchartsResult_selecBrowsevolumetMap;
	/**
	 * 查询各个浏览室人数List集合表示（柱状图）
	 * @return
	 */
	private List<String> EchartsResult_selectReadRoomNameListHistogram;
	/**
	 *查询各个浏览室浏览量List集合表示（柱状图）
	 * @return
	 */
	private List<String> EchartsResult_selecBrowsevolumetListHistogram;
	
	
	public List<String> getEchartsResult_selectReadRoomNameList() {
		return EchartsResult_selectReadRoomNameList;
	}
	public void setEchartsResult_selectReadRoomNameList(List<String> echartsResult_selectReadRoomNameList) {
		EchartsResult_selectReadRoomNameList = echartsResult_selectReadRoomNameList;
	}
	
	public List<Map<String, Object>> getEchartsResult_selectReadRoomPeopleMap() {
		return EchartsResult_selectReadRoomPeopleMap;
	}
	public void setEchartsResult_selectReadRoomPeopleMap(List<Map<String, Object>> echartsResult_selectReadRoomPeopleMap) {
		EchartsResult_selectReadRoomPeopleMap = echartsResult_selectReadRoomPeopleMap;
	}
	
	public List<Map<String, Object>> getEchartsResult_selecBrowsevolumetMap() {
		return EchartsResult_selecBrowsevolumetMap;
	}
	public void setEchartsResult_selecBrowsevolumetMap(List<Map<String, Object>> echartsResult_selecBrowsevolumetMap) {
		EchartsResult_selecBrowsevolumetMap = echartsResult_selecBrowsevolumetMap;
	}
	
	public List<String> getEchartsResult_selectReadRoomNameListHistogram() {
		return EchartsResult_selectReadRoomNameListHistogram;
	}
	public void setEchartsResult_selectReadRoomNameListHistogram(
			List<String> echartsResult_selectReadRoomNameListHistogram) {
		EchartsResult_selectReadRoomNameListHistogram = echartsResult_selectReadRoomNameListHistogram;
	}
	
	
	public List<String> getEchartsResult_selecBrowsevolumetListHistogram() {
		return EchartsResult_selecBrowsevolumetListHistogram;
	}
	public void setEchartsResult_selecBrowsevolumetListHistogram(
			List<String> echartsResult_selecBrowsevolumetListHistogram) {
		EchartsResult_selecBrowsevolumetListHistogram = echartsResult_selecBrowsevolumetListHistogram;
	}
	
	
	
}
