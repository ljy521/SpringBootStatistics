package com.ysd.util;
/**
 * 本地日志枚举
 * @author 李俊逸
 * @date  2019-9-17 15:19
 */
public enum LogEnum {
	BUSSINESS("bussiness"),
	 
    PLATFORM("platform"),
 
    DB("db"),
 
    EXCEPTION("exception"),
    ;
 
 
    private String category;
 
 
    LogEnum(String category) {
        this.category = category;
    }
 
    public String getCategory() {
        return category;
    }
 
    public void setCategory(String category) {
        this.category = category;
    }
}
