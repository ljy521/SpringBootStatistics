
package com.ysd.util;

import com.ysd.entity.Admin;
import com.ysd.entity.Student;
import com.ysd.entity.Teacher;

/**
 * 这个类用于登录信息的公共类
 * 
 * @author ChirsGod
 *
 */
public class Result {
	private Boolean success;// 成功状态
	private String msg;// 写入信息
	private Integer isLockout;// 用于登录用户是否锁定的状态
	private Student student;// 登录成功以后,存储该学生的对象信息
	private Teacher teacher;// 登录成功以后,存储该教室的对象信息 private Integer isLockout;// 用于登录用户是否锁定的状态
	private Object arr;//任意元素属性
	private Admin admin;//登录成功用来存储管理员信息

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getIsLockout() {
		return isLockout;
	}

	public void setIsLockout(Integer isLockout) {
		this.isLockout = isLockout;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Object getArr() {
		return arr;
	}

	public void setArr(Object arr) {
		this.arr = arr;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Result(Boolean success, String msg, Integer isLockout, Student student, Teacher teacher, Object arr,
			Admin admin) {
		super();
		this.success = success;
		this.msg = msg;
		this.isLockout = isLockout;
		this.student = student;
		this.teacher = teacher;
		this.arr = arr;
		this.admin = admin;
	}

	public Result() {
		super();
	}

	@Override
	public String toString() {
		return "Result [success=" + success + ", msg=" + msg + ", isLockout=" + isLockout + ", student=" + student
				+ ", teacher=" + teacher + ", arr=" + arr + ", admin=" + admin + "]";
	}



}
