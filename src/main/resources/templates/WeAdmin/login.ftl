<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>登录</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="shortcut icon" href="/SpringBootStatistics/favicon.ico"
	type="image/x-icon" />
<link rel="stylesheet" href="/SpringBootStatistics/css/font.css">
<link rel="stylesheet" href="/SpringBootStatistics/css/weadmin.css">
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>

</head>
<body class="login-bg">

	<div class="login">
		<div class="message">图书馆登录</div>
		<div id="darkbannerwrap"></div>

		<form method="post" class="layui-form" action="login">
		<span>${msg}</span>
			<input name="cardid" id="cardid" placeholder="请输入卡号" type="text"
				lay-verify="required|cardid" class="layui-input">
			<hr class="hr15">
			<select name="readroomid" id="readroomid" lay-verify="required">
				<option value="">请选择进入的资源室</option>
				<#list readroomList as readroom>
				<option value="${readroom.readroom_id}">${readroom.readroom_name}</option>
				</#list>
			</select>
			<hr class="hr15">
			<input class="loginin" value="刷卡" lay-submit lay-filter="login"
				style="width: 100%;" type="submit">
			<hr class="hr20">
		</form>
	</div>

	<script>
		//Demo
		var form;
		var layer;
		layui.use([ 'form', 'layer' ], function() {
			form = layui.form;
			layer = layui.layer;
			//监听提交
			form.on('submit(*)', function(data) {
				//layer.msg(JSON.stringify(data.field));
				layer.msg('欢迎您的登录');
			});
		});
	</script>
	<!-- 底部结束 -->
</body>
</html>