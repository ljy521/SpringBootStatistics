<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>统计图表-WeAdmin Frame型后台管理系统-WeAdmin 1.0</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Cache-Control" content="no-siteapp" />

<script type="text/javascript" src="/SpringBootStatistics/js/echarts.js"></script>
<script type="text/javascript"
	src="/SpringBootStatistics/js/jquery.min.js"></script>

<link rel="stylesheet" href="/SpringBootStatistics/layui/css/layui.css"
	media="all">
<script src="/SpringBootStatistics/layui/jquery-3.4.1.min.js"
	charset="utf-8"></script>
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>
<script src="/SpringBootStatistics/js/eleDel.js" type="text/javascript"
	charset="utf-8"></script>
</head>

<script type="text/javascript">
	$(function() {
		dtllnnbl();//学生人数男女比例
		dtllnnbllll();//学生浏览量男女比例
		dtllnnbxl();//查询各个浏览室人数学历List集合表示（柱状图）
		dtllnnbxllll();//查询各个浏览室浏览量学历List集合表示（柱状图）
		dtysrsxl();//查询各个浏览室院室人数List集合表示（柱状图）
		dtllyxlll();//查询各个浏览室院室浏览量List集合表示（柱状图）
	});
	//学生人数男女比例
	function dtllnnbl() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecMalefemaleratioListHistogram", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtllrs'));

						option = {
							title : {
								text : '人数男女比例',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '男', '女' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}

	//学生浏览量男女比例
	function dtllnnbllll() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecBrowseleratioListHistogram", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtllrslll'));

						option = {
							title : {
								text : '浏览量男女比例',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '男', '女' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}

	//查询各个浏览室人数学历List集合表示（柱状图）
	function dtllnnbxl() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecBrowseEducationListHistogram", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtllrsxl'));

						option = {
							title : {
								text : '学历浏览人数',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '中专', '大专', '本科' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}

	//查询各个浏览室浏览量学历List集合表示（柱状图）
	function dtllnnbxllll() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecseleraEducationListHistogram", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtllrsxllll'));

						option = {
							title : {
								text : '学历浏览量',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '中专', '大专', '本科' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}
	//查询各个浏览室院室人数List集合表示（柱状图）
	function dtysrsxl() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecNumberofbrowsingroomsList", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtysrsxl'));

						option = {
							title : {
								text : '院室人数',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '电子信息系', '机电系', '数控系' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}

	//查询各个浏览室院室浏览量List集合表示（柱状图）
	function dtllyxlll() {
		$
				.ajax({
					type : "post",
					async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
					url : "/SpringBootStatistics/statistics/selecseleraofbrowsingroomsList", //请求发送到StatisticsController处
					dataType : "json", //返回数据形式为json
					success : function(tt) {
						var myChart = echarts.init(document
								.getElementById('dtllyxlll'));

						option = {
							title : {
								text : '院室浏览量',
							},
							legend : {},
							tooltip : {},
							dataset : {
								dimensions : [ 'product', '电子信息系', '机电系', '数控系' ],
								source : tt
							},
							xAxis : {
								type : 'category'
							},
							yAxis : {},
							// Declare several bar series, each will be mapped
							// to a column of dataset.source by default.
							series : [ {
								type : 'bar'
							}, {
								type : 'bar'
							}, {
								type : 'bar'
							}, ]
						};
						// 使用刚指定的配置项和数据显示图表。
						myChart.setOption(option);
					}
				})
	}
</script>
<body>
<div class="layui-form">
	 <div class="layui-form-item">
    <label class="layui-form-label">区间查询</label>
    <div class="layui-input-inline">
      <input type="text" name="startData" class="layui-input" id="startData" placeholder="开始时间">
    </div>
    <div class="layui-input-inline">
    <input type="text" name="endData" class="layui-input" id="endData" placeholder="结束时间">
    </div>
    <div class="layui-input-inline">
  <button class="layui-btn" lay-filter="formDemo" onclick="tijiao()">立即提交</button>
    </div>
  </div>
	</div>
	<table>
		<tr>
			<td><div id="dtllrs" style="width: 590px; height: 400px;"></div></td>
			 <td><div id="dtllrsxl" style="width: 590px; height: 400px;"></div></td>
			
		</tr>
		<tr>
			<td><div id="dtllrslll" style="width: 590px; height: 400px;"></div></td>
			<td><div id="dtllrsxllll" style="width: 590px; height: 400px;"></div></td>
			
		</tr>
		<tr>
			<td><div id="dtysrsxl" style="width: 590px; height: 400px;"></div></td>
			<td><div id="dtllyxlll" style="width: 590px; height: 400px;"></div></td> 
		</tr>
	</table>
</body>
<script type="text/javascript">
	//立即提交
	function tijiao() {
		var startData = $("#startData").val();
		var endData = $("#endData").val();
		//学生人数男女比例
		$
				.post(
						'/SpringBootStatistics/statistics/selecMalefemaleratioListHistogram',
						{
							startData : startData,
							endData : endData
						}, function(tt) {
							var myChart = echarts.init(document
									.getElementById('dtllrs'));

							option = {
								title : {
									text : '人数男女比例',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '男', '女' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);

						})
		//学生浏览量男女比例
		$
				.post(
						'/SpringBootStatistics/statistics/selecBrowseleratioListHistogram',
						{
							startData : startData,
							endData : endData
						}, function(tt) {
							var myChart = echarts.init(document
									.getElementById('dtllrslll'));

							option = {
								title : {
									text : '浏览量男女比例',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '男', '女' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);

						})

		////查询各个浏览室人数学历List集合表示（柱状图）				
		$
				.post(
						'/SpringBootStatistics/statistics/selecBrowseEducationListHistogram',
						{
							startData : startData,
							endData : endData
						},
						function(tt) {
							var myChart = echarts.init(document
									.getElementById('dtllrsxl'));

							option = {
								title : {
									text : '学历浏览人数',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '中专', '大专', '本科' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);

						})
		//查询各个浏览室浏览量学历List集合表示（柱状图）			
		$
				.post(
						'/SpringBootStatistics/statistics/selecseleraEducationListHistogram',
						{
							startData : startData,
							endData : endData
						},
						function(tt) {

							var myChart = echarts.init(document
									.getElementById('dtllrsxllll'));

							option = {
								title : {
									text : '学历浏览量',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '中专', '大专', '本科' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);
						})
		//查询各个浏览室院室人数List集合表示（柱状图）			
		$
				.post(
						'/SpringBootStatistics/statistics/selecNumberofbrowsingroomsList',
						{
							startData : startData,
							endData : endData
						}, function(tt) {

							var myChart = echarts.init(document
									.getElementById('dtysrsxl'));

							option = {
								title : {
									text : '院室人数',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '电子信息系', '机电系',
											'数控系' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);
						})
		//查询各个浏览室院室浏览量List集合表示（柱状图）	
		$
				.post(
						'/SpringBootStatistics/statistics/selecseleraofbrowsingroomsList',
						{
							startData : startData,
							endData : endData
						}, function(tt) {

							var myChart = echarts.init(document
									.getElementById('dtllyxlll'));

							option = {
								title : {
									text : '院室浏览量',
								},
								legend : {},
								tooltip : {},
								dataset : {
									dimensions : [ 'product', '电子信息系', '机电系',
											'数控系' ],
									source : tt
								},
								xAxis : {
									type : 'category'
								},
								yAxis : {},
								// Declare several bar series, each will be mapped
								// to a column of dataset.source by default.
								series : [ {
									type : 'bar'
								}, {
									type : 'bar'
								}, {
									type : 'bar'
								}, ]
							};
							// 使用刚指定的配置项和数据显示图表。
							myChart.setOption(option);
						})

	}
</script>


<script>
	layui.use('laydate', function() {
		var laydate = layui.laydate;

		//执行一个laydate实例
		laydate.render({
			elem : '#startData' //指定元素
		});
		laydate.render({
			elem : '#endData' //指定元素
		});
	});
</script>
</html>