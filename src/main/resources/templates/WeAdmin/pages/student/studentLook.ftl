<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>学生信息查看</title>
<link rel="stylesheet" href="/SpringBootStatistics/layui/css/layui.css"
	media="all">
<script src="/SpringBootStatistics/layui/jquery-3.4.1.min.js"
	charset="utf-8"></script>
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>
</head>
<body>
	<div class="layui-form">
		<div class="layui-form-item">
			<label class="layui-form-label">学生姓名</label>
			<div class="layui-input-block">
				<input type="text" name="" id="" value="${student.student_name?if_exists}" disabled="disabled" autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">学生卡号</label>
			<div class="layui-input-block">
				<input type="text" name="" id="" value="${student.student_cardid?if_exists}" disabled="disabled" autocomplete="off" class="layui-input">
			</div>
		</div>
	</div>
	<table class="layui-table">
  <colgroup>
    <col width="150">
    <col width="200">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>卡号</th>
      <th>资源教室</th>
      <th>进入时间</th>
      <th>离开时间</th>
    </tr> 
  </thead>
  <tbody>
  <#list student.userlogList as userlog>
    <tr>
      <td>${userlog.userlog_cardid}</td>
      <td>${userlog.userlog_readroomid}</td>
      <td>${userlog.userlog_intime}</td>
      <td>${userlog.userlog_outtime}</td>
    </tr>
    </#list>
  </tbody>
</table>
</body>
</html>