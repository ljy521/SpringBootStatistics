<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>欢迎进入</title>
<link rel="stylesheet" href="/SpringBootStatistics/layui/css/layui.css"
	media="all">
<script src="/SpringBootStatistics/layui/jquery-3.4.1.min.js"
	charset="utf-8"></script>
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript">
	function logOut() {
		var cardid = $('#cardid').text();
	    $.post('/SpringBootStatistics/admin/logout', {
	    	cardid : cardid
		}, function(res) {
			if(res){
				alert("退出成功");
			}
			else{
				alert("退出失败");
			}
		}, 'json');
	}
</script>
</head>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<div class="layui-logo">欢迎进入系统</div>
			<ul class="layui-nav layui-layout-right">
				<li class="layui-nav-item"><a href="javascript:;"> ${name}
				</a></li>
				<li class="layui-nav-item"><a href="javascript:;"
					onclick="logOut()">退出</a></li>
			</ul>
		</div>

		<div class="layui-side layui-bg-black">
			<div class="layui-side-scroll">
				<!-- 左侧导航区域（可配合layui已有的垂直导航） -->

			</div>
		</div>

		<div class="layui-body">
			<!-- 内容主体区域 -->
			<div style="padding: 15px;">${msg}</div>
			<div style="padding: 15px;" id="cardid">${cardid}</div>
		</div>

		<div class="layui-footer">
			<!-- 底部固定区域 -->
			欢迎来到图书馆
		</div>
	</div>
	<script>
		//JavaScript代码区域
		layui.use('element', function() {
			var element = layui.element;

		});
	</script>
</body>
</html>