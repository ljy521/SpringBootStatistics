<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>教师列表</title>
<meta name="renderer" content="webkit" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="stylesheet" href="/SpringBootStatistics/css/font.css">
<link rel="stylesheet" href="/SpringBootStatistics/css/weadmin.css">
<link rel="stylesheet" href="/SpringBootStatistics/layui/css/layui.css" media="all">
<script src="/SpringBootStatistics/layui/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>
<script src="/SpringBootStatistics/js/eleDel.js" type="text/javascript" charset="utf-8"></script>
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
		  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
		  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
<script type="text/html" id="titleTpl">
  {{#  if(d.teacher_sex == 1){ }}
    男
  {{#  } else { }}
   女
  {{#  } }}
</script>
<script type="text/html" id="titleTpl2">
  {{#  if(d.teacher_status >0){ }}
    {{d.readroom.readroom_name }}
  {{#  } else { }}
   未在阅览室中
  {{#  } }}
</script>
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
</script>
<script type="text/javascript">
	$(function() {
		init();
	});
	function init() {
		layui.use(['table','layer','form'], function(){
			  var table = layui.table;
			  var layer = layui.layer;
			  var form = layui.form;
			  //第一个实例
			  table.render({
			    elem: '#demo'
			    ,url: '/SpringBootStatistics/teacher/findTeacherPage' //数据接口
			    ,page: true //开启分页
			    ,toolbar: 'default'
			    ,cols: [[ //表头
			      {field: 'teacher_id', title: 'ID', width:80, sort: true, fixed: 'left'}
			      ,{field: 'teacher_name', title: '姓名', width:80}
			      ,{field: 'teacher_cardid', title: '卡号', width:120}
			      ,{templet: '#titleTpl', title: '性别', width:80, sort: true}
			      ,{templet: '<div>{{d.section.section_name}}</div>', title: '科室', width: 150, sort: true}
			      ,{templet: '#titleTpl2', title: '状态', width: 120}
			      ,{field: 'teacher_remark', title: '备注', width: 250}
			      ,{fixed: 'right', width:170, align:'center', toolbar: '#barDemo'}
			    ]],
			    where :{
					text1: $('#text1').val(),
					num1: $('#num1').val(),
					num2: $('#num2').val(),
				}
			  });
				table.on('tool(test)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
					  var data = obj.data; //获得当前行数据
					  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
					  var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
					 
					  if(layEvent === 'detail'){ //查看
					    //do somehing
						  layer.open({
							  type: 2, 
							  content: '/SpringBootStatistics/admin/jumpTeacherLook?teacher_cardid='+data.teacher_cardid, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
						      title :'教师信息',
						      area: ['500px', '300px'],
						      maxmin :true
							}); 
					  }
					});
			});
	}
</script>

</head>

<body>
<div class="layui-form">
 <div class="layui-form-item">
    <label class="layui-form-label">姓名</label>
    <div class="layui-input-block">
      <input type="text" name="text1" id="text1" placeholder="请输入姓名" autocomplete="off" class="layui-input">
    </div>
  </div>
   <div class="layui-form-item">
    <label class="layui-form-label">性别</label>
    <div class="layui-input-block">
       <select name="num1" id="num1">
        <option value=""></option>
        <option value="1">男</option>
        <option value="0">女</option>
      </select>
    </div>
  </div>
     <div class="layui-form-item">
    <label class="layui-form-label">科室</label>
    <div class="layui-input-block">
       <select name="num2" id="num2">
        <option value=""></option>
        <#list sectionList as section>
        <option value="${section.section_id}">${section.section_name}</option>
        </#list>
      </select>
    </div>
  </div>
   <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" onclick="init()">搜索</button>
    </div>
  </div>
<button type="button" class="layui-btn" id="test1">
		<i class="layui-icon">&#xe67c;</i>导入信息
	</button>
<a href="/SpringBootStatistics/teacher/teacherExport" class="layui-btn">导出信息</a>
</div>
	<table id="demo" lay-filter="test"></table>
</body>
	<script>
layui.use('upload', function(){
  var upload = layui.upload;
   
  //执行实例
  var uploadInst = upload.render({
    elem: '#test1' //绑定元素
    ,url: '/SpringBootStatistics/teacher/teacherImport' //上传接口
    ,accept:'file'
    ,done: function(res){
      //上传完毕回调
      console.log(res);
      if(res.success){
    	  alert("导入成功");
    	  location.reload();
      }
      else{
    	  alert(res.msg);
    	  location.reload();
      }
    }
    ,error: function(){
      //请求异常回调
    }
  });
});
</script>
</html>