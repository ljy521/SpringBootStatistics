<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>欢迎页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="/SpringBootStatistics/css/font.css">
<link rel="stylesheet" href="/SpringBootStatistics/css/weadmin.css">

<script type="text/javascript"
	src="/SpringBootStatistics/js/echarts.js"></script>
<script type="text/javascript"
	src="/SpringBootStatistics/js/jquery.min.js"></script>
	
<script type="text/javascript">
	$(function() {
		dtllrs();
		dtllcs();
		/* dtllrszzt();
		dtllcszzt(); */
	});
	//学校各大场所前一天总浏览人数（饼状图）
	function dtllrs() {
		$.ajax({
			type : "post",
			async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
			url : "/SpringBootStatistics/statistics/selectReadRoomPeople", //请求发送到StatisticsController处
			dataType : "json", //返回数据形式为json
			success : function(tt) {
				var myChart = echarts.init(document.getElementById('dtllrs'));
				option = {
					title : {
						text : '学校各大场所前一天总浏览人数',
						subtext : '真实统计',
						x : 'center'
					},
					tooltip : {
						trigger : 'item',
						formatter : "{a} <br/>{b} : {c} ({d}%)"
					},
					legend : {
						orient : 'vertical',
						left : 'left',
						data : tt.echartsResult_selectReadRoomNameList,
					},
					series : [ {
						name : '访问来源',
						type : 'pie',
						radius : '55%',
						center : [ '50%', '60%' ],
						data : tt.echartsResult_selectReadRoomPeopleMap,
						itemStyle : {
							emphasis : {
								shadowBlur : 10,
								shadowOffsetX : 0,
								shadowColor : 'rgba(0, 0, 0, 0.5)'
							}
						}
					} ]
				};
				// 使用刚指定的配置项和数据显示图表。
				myChart.setOption(option);
			}
		})
	}
	//学校各大场所前一天总浏览人数（柱状图）
	function dtllrszzt() {
		$.ajax({
			type : "post",
			async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
			url : "/SpringBootStatistics/statistics/selectReadRoomPeopleHistogram", //请求发送到StatisticsController处
			dataType : "json", //返回数据形式为json
			success : function(tt) {
				var myChart = echarts.init(document.getElementById('dtllrszzt'));
				option = {
						title : {
							text : '学校各大场所前一天总浏览人数',
							subtext : '真实统计',
							x : 'center'
						},
					    xAxis: {
					        type: 'category',
					        data:  tt.echartsResult_selectReadRoomNameList,
					    },
					    yAxis: {
					        type: 'value'
					    },
					    series: [{
					        data: tt.echartsResult_selectReadRoomNameListHistogram,
					        type: 'bar'
					    }]
					};
				// 使用刚指定的配置项和数据显示图表。
				myChart.setOption(option);
			}
		})
	}
	//学校各大场所前一天总浏览量（饼状图）
	function dtllcs() {
		$.ajax({
			type : "post",
			async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
			url : "/SpringBootStatistics/statistics/selectReadRoomVolume", //请求发送到StatisticsController处
			dataType : "json", //返回数据形式为json
			success : function(tt) {
				var myChart = echarts.init(document.getElementById('dtlll'));
				option = {
					title : {
						text : '学校各大场所前一天总浏览量',
						subtext : '真实统计',
						x : 'center'
					},
					tooltip : {
						trigger : 'item',
						formatter : "{a} <br/>{b} : {c} ({d}%)"
					},
					legend : {
						orient : 'vertical',
						left : 'left',
						data : tt.echartsResult_selectReadRoomNameList,
					},
					series : [ {
						name : '访问来源',
						type : 'pie',
						radius : '55%',
						center : [ '50%', '60%' ],
						data : tt.echartsResult_selecBrowsevolumetMap,
						itemStyle : {
							emphasis : {
								shadowBlur : 10,
								shadowOffsetX : 0,
								shadowColor : 'rgba(0, 0, 0, 0.5)'
							}
						}
					} ]
				};
				// 使用刚指定的配置项和数据显示图表。
				myChart.setOption(option);
			}
		})
	}
	
	
	//学校各大场所前一天总浏览量（柱状图）
	function dtllcszzt() {
		$.ajax({
			type : "post",
			async : true, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
			url : "/SpringBootStatistics/statistics/selectReadRoomVolumeHistogram", //请求发送到StatisticsController处
			dataType : "json", //返回数据形式为json
			success : function(tt) {
				var myChart = echarts.init(document.getElementById('dtlllzzt'));
				option = {
						title : {
							text : '学校各大场所前一天总浏览量',
							subtext : '真实统计',
							x : 'center'
						},
					    xAxis: {
					        type: 'category',
					        data:  tt.echartsResult_selectReadRoomNameList,
					    },
					    yAxis: {
					        type: 'value'
					    },
					    series: [{
					        data: tt.echartsResult_selecBrowsevolumetListHistogram,
					        type: 'bar'
					    }]
					};
				// 使用刚指定的配置项和数据显示图表。
				myChart.setOption(option);
			}
		})
	}
</script>
</head>

<body>
	<blockquote class="layui-elem-quote">欢迎使用统计资源模板</blockquote>
	<table>
		<tr>
			<td><div id="dtllrs" style="width: 400px; height: 400px;"></div></td>
			<td><div id="dtlll" style="width: 400px; height: 400px;"></div></td>
		</tr>
		
		
		<tr>
			<td><div id="dtllrszzt" style="width: 400px; height: 400px;"></div></td>
			<td><div id="dtlllzzt" style="width: 400px; height: 400px;"></div></td>
		</tr>
	</table>




</body>
</html>