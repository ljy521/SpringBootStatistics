<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>管理员登录</title>
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="shortcut icon" href="/SpringBootStatistics/favicon.ico"
	type="image/x-icon" />
<link rel="stylesheet" href="/SpringBootStatistics/css/font.css">
<link rel="stylesheet" href="/SpringBootStatistics/css/weadmin.css">
<script src="/SpringBootStatistics/layui/layui.js" charset="utf-8"></script>

</head>
<body class="login-bg">

	<div class="login">
		<div class="message">管理员登录</div>
		<div id="darkbannerwrap"></div>

		<form method="post" class="layui-form" action="/SpringBootStatistics/admin/jumpIndex">
		<span>${msg}</span>
			<input name="admin_username" id="admin_username" placeholder="请输入账号" type="text"
				lay-verify="required" class="layui-input">
			<hr class="hr15">
			 <input type="password" name="admin_password" id="admin_password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
			<hr class="hr15">
			<input class="loginin" value="登录" lay-submit lay-filter="login"
				style="width: 100%;" type="submit">
			<hr class="hr20">
		</form>
	</div>

	<script>
		//Demo
		var form;
		var layer;
		layui.use([ 'form', 'layer' ], function() {
			form = layui.form;
			layer = layui.layer;
			//监听提交
			form.on('submit(*)', function(data) {
				//layer.msg(JSON.stringify(data.field));
				layer.msg('欢迎您的登录');
			});
		});
	</script>
	<!-- 底部结束 -->
</body>
</html>